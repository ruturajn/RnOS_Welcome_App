<div align="center">

# The Welcome App for RnOS

<img src="./assets/Welcome-1.png" width="708" height="526">

</div>

## Details

This is the **welcome application** for *RnOS*, that autostarts when the user logs in. Following is a brief explanation of it's functionality and behaviour.

<div align="center">
<img src="./assets/Welcome-3.png" width="411" height="306"> <img src="./assets/Welcome-4.png" width="411" height="306">
</div>

- As described earlier, when the application launches, you will be greeted with the logo for RnOS, and a short description about it. From here, you can either
choose to directly **launch calamares** for installation, read more about *RnOS*, or view the keybindings for **Qtile**. Finally, you can also remove this
application from the `autostart` directory, to prevent it from autostarting.
- If you wish to learn more about RnOS, proceed to clicking the `about` button, which will then open a window, containing some more information about the
distribution.


- Similarly, if you choose to view the keybindings configured for *Qtile*, click on the `View Keybindings` button, after which you will be greeted by a window, 
displaying the current keyboard shortcuts.


- Also, if you wish to explore the distribution, in the live environment you can click on the `Explore` button, which will close the main window, and
display a prompt, asking whether you would like to see the theme changing script (that utilizes `pywal`) in action.

- Finally, if you would like to do so, click on the `Sure` button, that will lead you to a `rofi` window, asking you to choose a wallpaper, from the
wallpapers available (stored at `/usr/share/Wallpapers`).

## Conclusion
That sums up the all the functions, in the ***Welcome App***. If if you are facing a bug or any problems, please feel free to open an `Issue` or a `Pull Request`.
