#!/usr/bin/env python3

# Author : Ruturajn <nanotiruturaj@gmail.com>

# Import necessary libraries
import os, sys

# Add ~/.config/qtile to path
home=os.path.expanduser("~")
sys.path.append(home + "/.config/qtile")

from Keybindings import keys

# Iterate for each element in the `keys` array.
for keybind in keys:
    final_mod = ""
    for element in keybind.modifiers:
        if element:
            if element == "mod4":
                element = "Super"
            if element == "mod1":
                element = "Alt"
            if element == "control":
                element = "Control"
            if element == "shift":
                element = "Shift"
            if final_mod:
                final_mod = final_mod + "+" + element
            else:
                final_mod = element
    if len(keybind.key) == 1:
        keybind.key = keybind.key.upper()
    if keybind.key == "space":
        keybind.key = "Space"
    if final_mod:
        num_spaces = 24 - len(final_mod + "+" + keybind.key)
        print(final_mod + "+" + keybind.key + " "*num_spaces + ": " + keybind.desc)
    else:
        num_spaces = 24 - len(final_mod + keybind.key)
        print(final_mod + keybind.key + " "*num_spaces + ": " + keybind.desc)

